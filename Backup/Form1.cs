﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Backup
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        public string path;
        public string localdest;
        public string Fullname;

        struct FtpSetting
        {
            public string Server { get; set; }
            public string ID { get; set; }
            public string Password { get; set; }
            public string FileName { get; set; }
            public string FullName { get; set; }
        }

        FtpSetting _inputParameter;

        private void backgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            if (checkBox2.Checked == true)
            {
                string fileName = ((FtpSetting)e.Argument).FileName;
                string fullName = ((FtpSetting)e.Argument).FullName;
                string id = ((FtpSetting)e.Argument).ID;
                string password = ((FtpSetting)e.Argument).Password;
                string server = ((FtpSetting)e.Argument).Server;
                FtpWebRequest request = (FtpWebRequest)WebRequest.Create(new Uri(string.Format("{0}/{1}", server, fileName)));
                request.Method = WebRequestMethods.Ftp.UploadFile;
                request.Credentials = new NetworkCredential(id, password);
                Stream ftpStream = request.GetRequestStream();
                FileStream fs = File.OpenRead(fullName);
                byte[] buffer = new byte[1024];
                double total = (double)fs.Length;
                int byteRead = 0;
                double read = 0;
                do
                {
                    if (!backgroundWorker.CancellationPending)
                    {
                        byteRead = fs.Read(buffer, 0, 1024);
                        ftpStream.Write(buffer, 0, byteRead);
                        read += (double)byteRead;
                        double percentage = read / total * 100;
                        backgroundWorker.ReportProgress((int)percentage);
                    }
                }
                while (byteRead != 0);
                fs.Close();
                ftpStream.Close();
            }
            else
            {
                string fileName = ((FtpSetting)e.Argument).FileName;
                string fullName = ((FtpSetting)e.Argument).FullName;
                string id = ((FtpSetting)e.Argument).ID;
                string password = ((FtpSetting)e.Argument).Password;
                string server = ((FtpSetting)e.Argument).Server;
                FtpWebRequest request = (FtpWebRequest)WebRequest.Create(new Uri(string.Format("{0}/{1}", server, fileName)));
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = new NetworkCredential(id, password);

                FtpWebRequest request1 = (FtpWebRequest)WebRequest.Create(new Uri(string.Format("{0}/{1}", server, fileName)));
                request1.Method = WebRequestMethods.Ftp.GetFileSize;
                request1.Credentials = new NetworkCredential(id, password);
                FtpWebResponse response = (FtpWebResponse)request1.GetResponse();
                double total = response.ContentLength;
                response.Close();

                FtpWebRequest request2 = (FtpWebRequest)WebRequest.Create(new Uri(string.Format("{0}/{1}", server, fileName)));
                request2.Method = WebRequestMethods.Ftp.GetDateTimestamp;
                request2.Credentials = new NetworkCredential(id, password);
                FtpWebResponse response2 = (FtpWebResponse)request2.GetResponse();
                DateTime modify = response2.LastModified;
                response2.Close();

                Stream ftpstream = request.GetResponse().GetResponseStream();
                FileStream fs = new FileStream(localdest, FileMode.Create);

                byte[] buffer = new byte[1024];
                int byteRead = 0;
                double read = 0;
                do
                {
                    byteRead = ftpstream.Read(buffer, 0, 1024);
                    fs.Write(buffer, 0, byteRead);
                    read += (double)byteRead;
                    double percentage = read / total * 100;
                    backgroundWorker.ReportProgress((int)percentage);
                }
                while (byteRead != 0);
                ftpstream.Close();
                fs.Close();
            }
        }

        private void backgroundWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            lblStatus.Text = $"{e.ProgressPercentage} %";
            progressBar.Value = e.ProgressPercentage;
            progressBar.Update();
        }
        private void backgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            string message = "Hoàn thành !!!";
            string title = string.Empty;
            MessageBoxButtons buttons = MessageBoxButtons.OK;
            DialogResult result = MessageBox.Show(message, title, buttons);
            if (result == DialogResult.OK)
            {
                progressBar.Value = 0;
                lblStatus.Text = "0%";
            }

        }

        private void btnUpload_Click(object sender, EventArgs e)
        {
            if (checkBox2.Checked == true)
            {
                FileInfo fi = new FileInfo(txtFile.Text);
                _inputParameter.ID = txtID.Text;
                _inputParameter.Password = txtPassword.Text;
                _inputParameter.Server = txtServer.Text;
                _inputParameter.FileName = fi.Name;
                _inputParameter.FullName = fi.FullName;
            }
            if (checkBox1.Checked == true)
            {
                _inputParameter.ID = txtID.Text;
                _inputParameter.Password = txtPassword.Text;
                _inputParameter.Server = txtServer.Text;
                _inputParameter.FileName = txtFile.Text;
                path = @"C:\Users\savio\Desktop\New folder\LTM\";
                localdest = path + @"" + _inputParameter.FileName;
                Fullname = _inputParameter.Server + @"/" + _inputParameter.FileName;

            }
            backgroundWorker.RunWorkerAsync(_inputParameter);
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked == true)
            {
                btn1.Enabled = true;
                btnBroswer.Enabled = false;
                checkBox2.Enabled = true;
                checkBox2.Checked = false;
                btn1.Text = @"Restore";
                txtFile.Enabled = true;
                checkBox1.Enabled = false;
                lblStatus.Visible = true;
                lblStatus.Text = @"0%";
            }
        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox2.Checked == true)
            {
                btn1.Enabled = true;
                btnBroswer.Enabled = true;
                checkBox1.Enabled = true;
                checkBox1.Checked = false;
                btn1.Text = @"Backup";
                txtFile.Enabled= false;
                checkBox2.Enabled = false;
                lblStatus.Visible = true;
                lblStatus.Text = @"0%";
            }
        }

        private void btnBroswer_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog ofd = new OpenFileDialog() { Multiselect = false, ValidateNames = true, Filter = "All files|*.*" })
            {
                if (ofd.ShowDialog() == DialogResult.OK)
                {
                    txtFile.Text = ofd.FileName;
                }
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            btn1.Enabled = false;
            btnBroswer.Enabled = false;
            txtFile.Enabled = false;
            lblStatus.Visible = false;
        }
    }
}